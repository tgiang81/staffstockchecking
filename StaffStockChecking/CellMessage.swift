//
//  CellMessage.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//

import UIKit
import SwiftyJSON

class CellMessage: UITableViewCell {

    @IBOutlet weak var m_Image: UIImageView!
    @IBOutlet weak var m_Name: UILabel!
    @IBOutlet weak var m_Color: UILabel!
    @IBOutlet weak var m_Size: UILabel!
    @IBOutlet weak var m_Westgate: UILabel!
    @IBOutlet weak var m_Status: UILabel!
    
    let queryService = APIManager()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
