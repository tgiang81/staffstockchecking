//
//  AppDelegate.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    //Keeping reference of iOS default UITabBarController.
    let referenceUITabBarController = CustomTabbarController()
    
    //District ["District-Book"]
    //DIN 1451 Com ["DIN1451Com-Engschrift"]
    
    //MARK: App Life Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //This is important.
        //Setup Application Window
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        fnShowLogin()
        
        //create folder products
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let path = NSURL(fileURLWithPath: paths[0] as String)
        let fullPath = path.appendingPathComponent( Constants.APIDetails.FOLDER_PRODUCT )
        
        var isDir : ObjCBool = true
        
        if FileManager.default.fileExists(atPath: fullPath!.path, isDirectory: &isDir)
        {
            
        }
        else
        {
            print("File does not exist")
            do {
                try FileManager.default.createDirectory(atPath: fullPath!.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
            
        }
        
        return true
    }
    
    func fnShowLogin() -> () {
        //Creating a storyboard reference
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginVCID")
        
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
        
    }
    
    func fnShowHome() -> () {
        self.referenceUITabBarController.selectedIndex = 0
        self.window?.rootViewController = self.referenceUITabBarController
        self.window?.makeKeyAndVisible()
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
