//
//  CellProduct.swift
//  TemplateGS
//
//  Created by Giang on 8/17/17.
//  Copyright © 2017 giangtu. All rights reserved.
//

import UIKit
import SwiftyJSON

class CellProduct: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var vViewValues: UIView!
    var myArrData :[JSON] = []
    var myIndexPath : IndexPath!
    
    var completeClick: (([String:Any])->())? = nil


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fnClick(sender: UIButton)  {
        
        let index = sender.tag - 10
        completeClick!([  "order": "\(index)",
            "store_id":"\(myArrData[index]["store_id"].stringValue)",
            "indexpath": "\(myIndexPath.row)",
            "store_name":"\(myArrData[index]["store_name"].stringValue)"])
    }
    
    /*
    {
    "stock" : "1",
    "store_id" : "100"
    }
    */
    func fnCreateSubViews( arrIn: [JSON], withIndexPath idP: IndexPath){
        //save index
        myIndexPath = idP
        myArrData = arrIn
        
        for v in vViewValues.subviews{
            v.removeFromSuperview()
        }
        
        var vTmp : UIButton? = nil
        
        let iCount = arrIn.count
        
        for i in 0...iCount - 1
        {
            let dic = arrIn[i]            
            let v: UIButton = UIButton.init()
            vViewValues.addSubview(v)
            v.translatesAutoresizingMaskIntoConstraints = false
            v.tag = 10 + i
            v.addTarget(self, action: #selector(fnClick), for: .touchUpInside)
            
            //v.setTitle("\(dic["stock"].stringValue)", for: .normal)
            
            let attributedString = NSMutableAttributedString(string: (dic["stock"].stringValue), attributes: [NSFontAttributeName:UIFont(name: "District-Book", size: 10)!, NSForegroundColorAttributeName: UIColor.black]
                
            )
            attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))

            v.setAttributedTitle(attributedString, for: .normal)
            v.backgroundColor = UIColor.clear

            v.setTitleColor(.black, for: .normal)
            
            let c2 = NSLayoutConstraint(item: v, attribute: .top, relatedBy: .equal, toItem: vViewValues, attribute: .top, multiplier: 1.0, constant: 0.0)
            let c3 = NSLayoutConstraint(item: v, attribute: .bottom, relatedBy: .equal, toItem: vViewValues, attribute: .bottom, multiplier: 1.0, constant: 0.0)
            NSLayoutConstraint.activate([c2, c3])
            
            if(i == 0)
            {
                let c1 = NSLayoutConstraint(item: v, attribute: .left, relatedBy: .equal, toItem: vViewValues, attribute: .left, multiplier: 1.0, constant: 0.0)
                NSLayoutConstraint.activate([c1])
            }
            else if(i == iCount - 1 )
            {
                
                let c2 = NSLayoutConstraint(item: v, attribute: .right, relatedBy: .equal, toItem: vViewValues, attribute: .right, multiplier: 1.0, constant: 0.0)
                
                NSLayoutConstraint.activate([c2])
            }
            
            if((vTmp) != nil)
            {
                let c3 = NSLayoutConstraint(item: v, attribute: .width, relatedBy: .equal, toItem: vTmp, attribute: .width, multiplier: 1.0, constant: 0.0)
                let c4 = NSLayoutConstraint(item: vTmp!  , attribute: .right, relatedBy: .equal, toItem: v, attribute: .left, multiplier: 1.0, constant: 0.0)
                
                NSLayoutConstraint.activate([c3,c4])
                
            }
            vTmp = v
        }

        self.setNeedsDisplay()
        //
    }
}
