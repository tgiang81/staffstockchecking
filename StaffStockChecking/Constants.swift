//
//  Constants.swift
//  StaffStockChecking
//
//  Created by Giang on 8/10/17.
//  Copyright © 2017 Hemang Shah. All rights reserved.
//
 
import Foundation

struct Constants {
 
    static var userName: String = ""
    static var barCodeNumber: String = ""
    static var BASE_IMAGE_URL: String = ""

    struct APIDetails {
        // MARK: List of Constants
        static let FOLDER_PRODUCT = "PRODUCTS"
        
        //static let BASE_URL = "https://private-anon-0a91513d64-v1shop.apiary-mock.com"
        static let BASE_URL = "https://api.shopcada.com"

        //Authentication
        static let API_LOGIN = "/v1/auth"
        
        //Products
        static func API_PRODUCT_DETAIL(_ productId: String) -> String {
            return  "/v1/products/" + productId
        }
        
        static func API_PRODUCT_SEARCH(_ name: String) -> String {
            return  "/v1/products?search=" + name
        }
        
        //Create a new check store request message - SEND a message
        static let API_MESSAGE_REQUEST =  "/v1/request"

        //Remote Check Stock
        //Retrieve the remote messages using the message Id
        static func API_MESSAGE_DETAIL(_ messageId: String) -> String {
            return  "/v1/request/" + messageId
        }
        
        
        //Stores List
        //Retrieve all the stores
        static let API_STORE_LIST =  "/v1/channels"
        

    }
}
