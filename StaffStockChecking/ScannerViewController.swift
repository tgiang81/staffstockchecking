//
//  ScannerViewController.swift
//  StaffStockChecking
//
//  Created by Giang on 8/12/17.
//  Copyright © 2017 Hemang Shah. All rights reserved.
//

import AVFoundation
import UIKit
import ACProgressHUD_Swift
import SwiftyJSON

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var vCamera: UIView!
    @IBOutlet weak var vKeyboard: UIView!
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnKeyboard: UIButton!
    @IBOutlet weak var tfSKU: UITextField!
    @IBOutlet weak var labelSKU: UILabel!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var imgvCamera: UIImageView!
    @IBOutlet weak var imgvKeyboard: UIImageView!
    @IBOutlet weak var lbNotify: UILabel!

    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var vPreview: UIView!
    let queryService = APIManager()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        var attributedString = NSMutableAttributedString(string: lbNotify.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        lbNotify.attributedText = attributedString

        
        captureSession = AVCaptureSession()

        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed();
            return;
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeCode128Code,
                                                  AVMetadataObjectTypeCode39Mod43Code,
                                                  AVMetadataObjectTypeCode39Code,
                                                  AVMetadataObjectTypeEAN8Code,
                                                  AVMetadataObjectTypeEAN13Code,
                                                  AVMetadataObjectTypePDF417Code]
        } else {
            failed()
        }

//
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        attributedString = NSMutableAttributedString(string: labelSKU.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        labelSKU.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: (buttonSearch.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        
        buttonSearch.setAttributedTitle(attributedString, for: .normal)
    }
    
    func failed() {
        captureSession = nil
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        
        let callFunction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { _ in
            self.fnCamera([])
            print("We can run a block of code." )

        }
        ac.addAction(callFunction)
        present(ac, animated: true)
        //captureSession = nil
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fnCamera([])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: readableObject.stringValue);
            //Save to global
            Constants.barCodeNumber = readableObject.stringValue
            self.fnSearchWithText(name: readableObject.stringValue)

        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func fnClose() {
        //dismiss scan view, comeback
        
    }
    
    
    @IBAction func fnCamera(_ sender: Any) {
        
        if (captureSession?.isRunning == false) {
            if(previewLayer == nil)
            {
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
                
                previewLayer.frame = vPreview.frame;
                
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                
                vCamera.layer.addSublayer(previewLayer);
            }
            captureSession.startRunning();
        }

        imgvCamera.image = UIImage.init(named: "ic_camera")
        imgvKeyboard.image = UIImage.init(named: "ic_keyboard_inactive")
        
        vCamera.isHidden = false
        vKeyboard.isHidden = true
    }
    
    @IBAction func fnKeyboard(_ sender: Any) {
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning();
        }

        imgvCamera.image = UIImage.init(named: "ic_camera_inactive")
        imgvKeyboard.image = UIImage.init(named: "ic_keyboard")
        
        vCamera.isHidden = true
        vKeyboard.isHidden = false
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func fnSearchSKU(_ sender: Any) {
        //Search API...have result...show ProductVC
        if tfSKU.text != "" {
            fnSearchWithText(name: tfSKU.text!)
        }
    }
    
    func fnSearchWithText(name: String) {
        
        let trimmedString =  name.trimmingCharacters(in: NSCharacterSet.whitespaces)
        
        if( trimmedString == "" )
        { return }
        
        let progressView = ACProgressHUD.shared
        progressView.progressText = "Searching..."
        progressView.showHUD()
        
        queryService.getProductFind(searchTerm: trimmedString, completion: { (results) in
            //Check error code/???
            
            //hide progress
            ACProgressHUD.shared.hideHUD()
            
            print(results)
            
            let arr = results["products"].arrayValue
            //Search successful -> Navigate to Product
            if (arr.count > 0)
            {
                //take the first product to view
                let json = arr[0].dictionaryValue
                
                guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
                
                let fileUrl = documentsDirectoryUrl.appendingPathComponent(  Constants.APIDetails.FOLDER_PRODUCT + "/" + (json["id"]?.stringValue)! + ".json")
                
                UserDefaults.standard.set( (json["id"]?.stringValue)! + ".json" , forKey: "selectedProduct")
                
                print(fileUrl.absoluteString);
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: arr[0].dictionaryObject ?? [] , options: [])
                    try data.write(to: fileUrl, options: [])
                } catch {
                    print(error)
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.referenceUITabBarController.selectedIndex = 1
            }else{
                //
                let ac = UIAlertController(title: "Error", message: "The product you trying to search is not exists.", preferredStyle: .alert)
                
                let callFunction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { _ in
                    self.fnCamera([])
                    print("We can run a block of code." )
                    
                }
                ac.addAction(callFunction)
                self.present(ac, animated: true)
                
            }
            
        } )
    }
}

extension ScannerViewController: UITextFieldDelegate {
    
    // For pressing return on the keyboard to dismiss keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        fnSearchSKU(textField)
        return true
    }
}

