//
//  APIManager.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//
import UIKit
import SwiftyJSON
import Toast_Swift
import ACProgressHUD_Swift

class APIManager: NSObject {
    
    typealias JSONDictionary = [String: Any]

    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
    var completionHandler: ( (JSON)->Void )?

    //LOGIN
    func getLogin(withAPIKey key:String, username: String, password: String, completion: @escaping (JSON) -> Void) {
        completionHandler = completion

        let strRequest = Constants.APIDetails.BASE_URL + Constants.APIDetails.API_LOGIN
        var mUrl: URL!
        mUrl = URL(string: strRequest)
        var request = URLRequest(url: mUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)

        request.httpMethod = "POST"
        request.addValue(key, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let parameters = [
            "username": username,
            "pass": password
            ] as [String : String]
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])

        request.httpBody = postData!
        processingData(request)
        
    }
    
    //PRODUCT_DETAIL
    func getProductDetail(productID: String, completion: @escaping  (JSON) -> Void) {
        completionHandler = completion

        let strRequest = Constants.APIDetails.BASE_URL + Constants.APIDetails.API_PRODUCT_DETAIL(productID)
        var mUrl: URL!
        mUrl = URL(string: strRequest)
        var request = URLRequest(url: mUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let apikey = UserDefaults.standard.value(forKey: "APIKEY") as! String
        
        request.setValue(apikey, forHTTPHeaderField: "Authorization")
        let token = UserDefaults.standard.value(forKey: "usertoken") as! String
        request.setValue(token, forHTTPHeaderField: "User-Token")
        
        processingData(request)
        
    }
    
    //LIST PRODUCT-> Take the first one
    func getProductFind(searchTerm: String, completion: @escaping (JSON) -> Void) {
        completionHandler = completion
        
        let escapedString = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)

        let strRequest = Constants.APIDetails.BASE_URL + Constants.APIDetails.API_PRODUCT_SEARCH(escapedString!)
        var mUrl: URL!
        mUrl = URL(string: strRequest)
        var request = URLRequest(url: mUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let apikey = UserDefaults.standard.value(forKey: "APIKEY") as! String
        
        request.setValue(apikey, forHTTPHeaderField: "Authorization")
        let token = UserDefaults.standard.value(forKey: "usertoken") as! String
        request.setValue(token, forHTTPHeaderField: "User-Token")
        
        processingData(request)
    }
    
    //SEND MESSAGE GGTT
    func postMessage(dicInput: [String:Any], completion: @escaping (JSON) -> Void) {
        completionHandler = completion

        let strRequest = Constants.APIDetails.BASE_URL + Constants.APIDetails.API_MESSAGE_REQUEST
        var mUrl: URL!
        mUrl = URL(string: strRequest)
        var request = URLRequest(url: mUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let apikey = UserDefaults.standard.value(forKey: "APIKEY") as! String
        request.httpMethod = "POST"

        request.setValue(apikey, forHTTPHeaderField: "Authorization")
        let token = UserDefaults.standard.value(forKey: "usertoken") as! String
        request.setValue(token, forHTTPHeaderField: "User-Token")

        let postData = try? JSONSerialization.data(withJSONObject: dicInput, options: [])
        
        request.httpBody = postData!

        
        processingData(request)
    }
    
    //GET MESSAGE
    func getMessage(messageID: String, completion: @escaping (JSON) -> Void) {
        completionHandler = completion
        let strRequest = Constants.APIDetails.BASE_URL + Constants.APIDetails.API_MESSAGE_DETAIL(messageID)
        var mUrl: URL!
        mUrl = URL(string: strRequest)
        var request = URLRequest(url: mUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let apikey = UserDefaults.standard.value(forKey: "APIKEY") as! String
        
        request.setValue(apikey, forHTTPHeaderField: "Authorization")
        let token = UserDefaults.standard.value(forKey: "usertoken") as! String
        request.setValue(token, forHTTPHeaderField: "User-Token")
        
        processingData(request)
    }
  
    //
    
    //STORE LIST - 
    //MARK: - Processing request
    func processingData(_ request: URLRequest){
        
        dataTask?.cancel()
        
        
        dataTask = defaultSession.dataTask(with : request) { data, response, error in
            defer { self.dataTask = nil }
            // 5
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            }
            else if let dataFromNetworking = data
            {
                
                let json = JSON(data: dataFromNetworking)
                
                if(json["error_code"] != JSON.null)
                {
                    let code = json["error_code"].intValue
                    switch code
                    {
                    case 401:
                        // You need to auto logout user and show the login screen.
                        DispatchQueue.main.async {

                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.fnShowLogin()
                            let vWindow = UIApplication.shared.delegate?.window
                            
                            let alert = UIAlertController(title: "Error",
                                                          message: "Invalid Username or Password. Please try again.",
                                                          preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction = UIAlertAction(title: "OK",
                                                             style: .cancel, handler: nil)
                            alert.addAction(cancelAction)
                            vWindow??.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                        break
                    case 403:
                        //You need to clear your Apikey data saved in NSUserDefault and show the login screen again with the Registration Code field again.
                        UserDefaults.standard.removeObject(forKey: "APIKEY")
                        DispatchQueue.main.async {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.fnShowLogin()
                            
                        }
                        break
                    default:
                        //Alert json["error"]
                        // toast with a specific duration and position

                        DispatchQueue.main.async {
//                            let vWindow = UIApplication.shared.delegate?.window
//                            vWindow??.makeToast(json["error"].stringValue, duration: 5.0, position: .top)
                            //hide progress
                            ACProgressHUD.shared.hideHUD()

                            self.completionHandler!(["message":json["error"]])

                        }
                        break
                    }

                }else{
                //Processing Data
                    DispatchQueue.main.async {
                        self.completionHandler!(json)
                    }
                }
                
            }
        }
        
        dataTask?.resume()
    }
    
}
