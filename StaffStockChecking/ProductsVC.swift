//
//  ProductsVC.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//

import SwiftyJSON
import ACProgressHUD_Swift
import SDWebImage

class ProductsVC: UIViewController,  UIGestureRecognizerDelegate{
    
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UITableView!
    @IBOutlet weak var vSendingMessage: UIView!
    @IBOutlet weak var vBlur: UIView!
    @IBOutlet weak var valSize: UILabel!
    @IBOutlet weak var valOutlet: UILabel!
    @IBOutlet weak var imgV: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbColour: UILabel!
    @IBOutlet weak var lbCost: UILabel!
    @IBOutlet weak var lbRack: UILabel!
    
    @IBOutlet weak var tbRelatedColor: UITableView!
    @IBOutlet weak var constrantTableRelated: NSLayoutConstraint!
    
    var iCount :Int = 0
    var storeid_inventory_selected : String = ""
    var my_product_id : String = ""
    var my_product_model : String = ""
    var my_product_colour : String = ""
    var my_product_image : String = ""
    var my_product_name : String = ""
    var arrRelatedProducts : [[String:String]] = []
    var arrTexts = [String]()
    
    @IBOutlet weak var constraintHeightMessage: NSLayoutConstraint!
    let iHeightMsg :Int = 282
    
    var elements: [JSON] = []
    var arrProductIDs : [String] = []
    
    fileprivate let estadeCellIdentifier = "estadeIdentifier"
    
    fileprivate let relatedIdentifier = "relatedIdentifier"
    
    let vBottom = UIView.init()
    
    let queryService = APIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        //MessageSend view
        let window = UIApplication.shared.keyWindow!
        vBottom.backgroundColor = .white
        window.addSubview(vBottom)
        vBottom.frame = CGRect.init(x: 0, y: window.frame.size.height-44, width: window.frame.size.width, height: 44)
        
        constraintHeightMessage.constant = 0
        
        collectionView.register(UINib.init(nibName: "CellProduct", bundle: nil), forCellReuseIdentifier: estadeCellIdentifier);
        
        tbRelatedColor.register(UINib.init(nibName: "CellRelated1", bundle: nil), forCellReuseIdentifier: relatedIdentifier);
        tbRelatedColor.alwaysBounceVertical = false
        
        //Swipe control
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        collectionView.addGestureRecognizer(swipeRight)
        swipeRight.delegate = collectionView as? UIGestureRecognizerDelegate
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.left
        collectionView.addGestureRecognizer(swipeDown)
        swipeDown.delegate = collectionView as? UIGestureRecognizerDelegate
        
        collectionView.alwaysBounceVertical = false
        
        imgV.image = nil
        lbName.text = ""
        lbColour.text = ""
        lbCost.text = ""
        lbRack.text = ""
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Fetch list Products id name files. // Delete file/folder.
        
        fnRefreshDir()
        
        self.fnHideSendingMessage()
    }
    
    func fnRefreshDir(){
        //setlect index for product
        arrProductIDs.removeAll()
        
        //Get list order product
        var strProductSelected = ""
        if let selectedProduct = UserDefaults.standard.string(forKey: "selectedProduct")
        {
            strProductSelected = selectedProduct
        }
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let folder = documentDirectory.appending("/" + Constants.APIDetails.FOLDER_PRODUCT)
        
        let folderURL:URL   = URL.init(fileURLWithPath: folder)
        
        if let urlArray = try? FileManager.default.contentsOfDirectory(at: folderURL ,
                                                                       includingPropertiesForKeys: [.contentModificationDateKey],
                                                                       options:.skipsHiddenFiles) {
            
            let arrResults = urlArray.map { url in
                (url.lastPathComponent, (try? url.resourceValues(forKeys: [.contentModificationDateKey]))?.contentModificationDate ?? Date.distantPast)
                }
                .sorted(by: { $0.1 > $1.1 }) // sort descending modification dates
                .map { $0.0 } // extract file names
            
            if arrResults.count > 0 {
                for i in 0...arrResults.count-1
                {
                    let fileName = arrResults[i]
                    if fileName == strProductSelected{
                        iCount = i
                    }
                    arrProductIDs.append(fileName)
                    print(fileName)
                }
                
                //refresh
                fnProductDetail()
                
            }else{
                fnProductDetail()
                
            }
            
        }
        
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: -Flip Left/Right to load Backward/Forward data.
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                fnSwipe(bLeft: true)
            case UISwipeGestureRecognizerDirection.left:
                fnSwipe(bLeft: false)
            default:
                break
            }
        }
    }
    
    func fnSwipe(bLeft : Bool)  {
        
        if bLeft {
            if iCount > 0 {
                self.view.slideInFrom(isLeft: true)
                iCount -= 1
            }
            else
            { return }
            
            
        }else{
            
            //load next
            if iCount < arrProductIDs.count-1 {
                self.view.slideInFrom(isLeft: false)
                iCount += 1
            }
            else
            { return }
        }
        UserDefaults.standard.set( (arrProductIDs[iCount]) , forKey: "selectedProduct")
        //change data content
        fnProductDetail()
    }
    
    /*
     "inventory": [{
     "store_id": "1",
     "store_name": "313",
     "location_name": "313",
     "stock": "1"
     }, {
     "store_id": "2",
     "store_name": "Wheelock",
     "location_name": "Wheelock",
     "stock": "1"
     }],
     */
    
    //Fetch related product
    func fnClick(dic: [String:String])  {
        //productID related
        
        print(index)
        
        let progressView = ACProgressHUD.shared
        progressView.progressText = "Loading..."
        progressView.showHUD()
        
        queryService.getProductDetail(productID: dic["id"]!) { (results) in
            //hide progress
            
            if let _ = results["product"].dictionaryObject {
                //get product detail...save
                guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
                
                let fileUrl = documentsDirectoryUrl.appendingPathComponent(  Constants.APIDetails.FOLDER_PRODUCT + "/" + (dic["id"])! + ".json")
                
                UserDefaults.standard.set( (dic["id"])! + ".json" , forKey: "selectedProduct")
                
                print(fileUrl.absoluteString);
                
                
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: results["product"].dictionaryObject ?? [:] , options: [])
                    try data.write(to: fileUrl, options: [])
                } catch {
                    print(error)
                }
                
                //if no error...refresh with new selectedProduct
                self.fnRefreshDir()
                ACProgressHUD.shared.hideHUD()
                
            }else{
                ACProgressHUD.shared.hideHUD()
                
                let alert = UIAlertController(title: "Error",
                                              message: "There is no product for this Color",
                                              preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: "OK",
                                                 style: .cancel, handler: nil)
                
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
    
    func fnProductDetail() {
        //refresh data
        elements.removeAll()
        collectionView.reloadData()
        //view product
        
        //refresh data:     //header values
        
        imgV.image = nil
        
        lbName.text = ""
        lbColour.text = ""
        lbCost.text =  ""
        lbRack.text =  ""
        
        
        guard let selectedProduct = UserDefaults.standard.string(forKey: "selectedProduct") else { return }
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let file = documentDirectory.appending("/" + Constants.APIDetails.FOLDER_PRODUCT + "/" + selectedProduct)
        let fileURL:URL   = URL.init(fileURLWithPath: file)
        
        
        //Read saved id-status message
        do {
            let data = try Data(contentsOf: fileURL, options: [])
            
            let object = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
            if let dictionary = object as? [String: AnyObject] {
                
                let jj = JSON(object: dictionary)
                //name colour images[0]
                my_product_colour = jj["colour"].stringValue
                
                let arrImgs = jj["images"].arrayValue
                
                if arrImgs.count > 0  {
                    my_product_image  = arrImgs[0].stringValue
                    
                    let url = URL(string: Constants.BASE_IMAGE_URL + my_product_image)
                    imgV.sd_setImage(with: url)
                    
                }
                
                my_product_name  = jj["name"].stringValue
                
                //header values
                lbName.text = my_product_name.uppercased()
                lbColour.text = my_product_colour.uppercased()
                lbCost.text =  String(format:"$%d",jj["variants"][0]["bulk_prices"]["1"].intValue) + " / " + String(format:"$%d",jj["variants"][0]["bulk_prices"]["3"].intValue) + " / " + String(format:"$%d",jj["variants"][0]["bulk_prices"]["6"].intValue).uppercased()
                lbRack.text =  (jj["variants"][0]["rack"].stringValue != "") ?"RACK: " + jj["variants"][0]["rack"].stringValue.uppercased() : "" 
                
                //
                var attributedString = NSMutableAttributedString(string: lbName.text!, attributes: [NSFontAttributeName:UIFont(name: "DIN1451Com-Engschrift", size: 12)!, NSForegroundColorAttributeName: UIColor.black]
                    
                )
                attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2), range: NSRange(location: 0, length: attributedString.length))
                
                lbName.attributedText = attributedString
                
                attributedString = NSMutableAttributedString(string: lbColour.text!)
                attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
                lbColour.attributedText = attributedString
                
                attributedString = NSMutableAttributedString(string: lbCost.text!)
                attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
                lbCost.attributedText = attributedString
                
                attributedString = NSMutableAttributedString(string: lbRack.text!)
                attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
                lbRack.attributedText = attributedString
                
                
                //build related product
                arrRelatedProducts = jj["related"].arrayObject as! [[String : String]]
                let iCount = arrRelatedProducts.count
                
                arrTexts.removeAll()
                
                //                lbRelated.arrObjects.removeAll()
                
                if iCount > 0
                {
                    for i in 0...iCount - 1
                    {
                        let dic = arrRelatedProducts[i]
                        
                        arrTexts.append(dic["colour"]!.uppercased())
                    }
                    
                }
                
                //
                constrantTableRelated.constant = CGFloat(iCount) * 30
                if (constrantTableRelated.constant < 58)
                {
                    constrantTableRelated.constant = 58
                }
                
                tbRelatedColor.reloadData()
                
                //End build related product
                
                let arrVariants = jj["variants"].arrayValue
                var dicProducts :[String: Any] = [:]
                var arrStoreNames :[String] = []
                var arrSizes :[[String:String]] = []
                let arrInventory = jj["variants"][0]["inventory"].arrayValue
                
                var indexMyStoreID = 0
                let myStoreID = UserDefaults.standard.string(forKey: "store_id")!
                
                for i in 0...arrInventory.count-1 {
                    let objInvent = arrInventory[i].dictionaryValue
                    let key = objInvent["store_name"]?.stringValue
                    
                    if(objInvent["store_id"]?.stringValue == myStoreID)
                    {
                        indexMyStoreID = i
                    }
                    
                    arrStoreNames.append(key!)
                    dicProducts[key!] = [] //key = storename
                }
                
                for i in 0...arrVariants.count-1 {
                    let obj = arrVariants[i]
                    //variant[0]
                    
                    var size = obj["options"]["size"].stringValue.uppercased()
                    if (size == "")
                    {
                        size = obj["options"]["Size"].stringValue.uppercased()
                    }
                    
                    arrSizes.append(["stock":size,
                                     "model":obj["model"].stringValue ])
                    
                    let arrInventori = obj["inventory"].arrayValue
                    
                    for j in 0...arrInventori.count-1 {
                        let invObj = arrInventori[j]
                        var arrTmp:[[String:String]] = dicProducts[ invObj["store_name"].stringValue ] as! [[String:String]]
                        //add
                        arrTmp.append( ["store_id" : invObj["store_id"].stringValue,
                                        "stock" : invObj["stock"].stringValue,
                                        "store_name" : invObj["store_name"].stringValue ] )
                        //set
                        dicProducts[ invObj["store_name"].stringValue ] = arrTmp
                        print("--------\(invObj["store_id"].stringValue) -------------")
                    }
                }
                print("------------------------")
                
                //first cell
                elements.append(JSON.init(["name": "Store",
                                           "value": arrSizes ] ))
                //My store
                let myObj :JSON = JSON.init(["name": arrStoreNames[indexMyStoreID],
                                             "value": dicProducts[ arrStoreNames[indexMyStoreID] ] ?? ""
                    ])
                elements.append(myObj)
                
                //Construct other Objects
                
                for i in 0...arrStoreNames.count-1 {
                    
                    if(i == indexMyStoreID)
                    {continue}
                    
                    let obj :JSON = JSON.init(["name": arrStoreNames[i],
                                               "value": dicProducts[ arrStoreNames[i] ] ?? ""
                        ])
                    elements.append(obj)
                }
                //reload table
                self.collectionView.reloadData()
                
                //print(elements)
            }
        } catch {
            print(error)
        }
    }
    
    //Send message
    @IBAction func fnSend(_ sender: Any) {
        
        let progressView = ACProgressHUD.shared
        progressView.progressText = "Sending..."
        progressView.showHUD()
        
        let receiver = UserDefaults.standard.string(forKey: "store_id")!
        queryService.postMessage(dicInput: [
            "product_id": my_product_id,
            "model": my_product_model, //qrcode
            "requester": receiver, //me
            "receiver": self.storeid_inventory_selected  //selected store_id
        ]) { (results) in
            
            //hide progress
            ACProgressHUD.shared.hideHUD()
            
            self.fnHideSendingMessage()
            
            if (results.dictionaryValue["request"] == nil)
            {
                let ac = UIAlertController(title: "Error", message: results.dictionaryValue["message"]?.stringValue, preferredStyle: .alert)
                
                let callFunction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { _ in
                }
                ac.addAction(callFunction)
                self.present(ac, animated: true)
                
                return
            }
            
            print(results)
            let sID = results.dictionaryValue["request"]!["txn_id"]
            let sStatus = results.dictionaryValue["request"]!["receiver_reply"]
            //read file
            
            // Get the url of message.json in document directory
            guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
            
            let user = UserDefaults.standard.string(forKey: "user")
            
            let fileUrl = documentsDirectoryUrl.appendingPathComponent("\(user!)_message.json")
            
            do {
                let data = try Data(contentsOf: fileUrl, options: [])
                guard var messageArr = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: String]] else { return }
                print(messageArr)
                
                
                messageArr.append(["txn_id":    sID.stringValue,
                                   "receiver_reply":sStatus.stringValue,
                                   "colour":    self.my_product_colour,
                                   "image":     self.my_product_image,
                                   "name":      self.my_product_name,
                                   "size":      self.valSize.text!,
                                   "outlet":    self.valOutlet.text!
                    ])
                //save again
                do {
                    let data = try JSONSerialization.data(withJSONObject: messageArr, options: [])
                    try data.write(to: fileUrl, options: [])
                } catch {
                    print(error)
                }
                
            } catch {
                let messageArr: NSMutableArray = []
                messageArr.add(["txn_id":    sID.stringValue,
                                "receiver_reply":sStatus.stringValue,
                                "colour":    self.my_product_colour,
                                "image":     self.my_product_image,
                                "name":      self.my_product_name,
                                "size":      self.valSize.text!,
                                "outlet":    self.valOutlet.text!
                    ])
                do {
                    let data = try JSONSerialization.data(withJSONObject: messageArr, options: [])
                    try data.write(to: fileUrl, options: [])
                } catch {
                    print(error)
                }
            }
        }
        
    }
    
    //MARK: - MessageView show/hide
    func fnShowSendingMessage() {
        let window = UIApplication.shared.keyWindow!
        self.vBottom.frame = CGRect.init(x: 0, y: window.frame.size.height-44, width: window.frame.size.width, height: 44)
        
        self.view.layoutIfNeeded() // Force lays of all subviews on root view
        
        UIView.animate(withDuration: 0.5) {
            self.constraintHeightMessage.constant = CGFloat(self.iHeightMsg)
            self.vBlur.isHidden = false
            self.view.layoutIfNeeded() // Force lays of all subviews on root view again.
            
        }
    }
    
    func fnHideSendingMessage(){
        self.view.layoutIfNeeded() // Force lays of all subviews on root view
        
        UIView.animate(withDuration: 0.35) {
            self.constraintHeightMessage.constant = 0
            self.view.layoutIfNeeded() // Force lays of all subviews on root view again.
        }
        self.vBlur.isHidden = true
        let window = UIApplication.shared.keyWindow!
        vBottom.frame = CGRect.init(x: 0, y: window.frame.size.height, width: window.frame.size.width, height: 44)
    }
    
    @IBAction func fnClose(_ sender: Any) {
        fnHideSendingMessage()
    }
    
    @IBAction func fnShowWebview(_ sender: Any) {

        guard let selectedProduct = UserDefaults.standard.string(forKey: "selectedProduct") else { return }

        let indexEndOfText = selectedProduct.index(selectedProduct.endIndex, offsetBy: -5)
        let strID = String(selectedProduct[..<indexEndOfText])
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "webVCID") as! WebVC
        controller.product_id = strID
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController!.present(controller, animated: true, completion: nil)
        
        //        present
    }
    
    func pressButton(button: UIButton) {
        let dic =  self.arrRelatedProducts[button.tag]
        print(dic)
        DispatchQueue.main.async {
            self.fnClick(dic: dic)
        }
        
    }
    
}

extension ProductsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == collectionView) {
            return elements.count
        }
        return arrTexts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == collectionView) {
            // Instantiate a cell
            let cell = tableView.dequeueReusableCell(withIdentifier: estadeCellIdentifier, for: indexPath) as! CellProduct;
            // Adding the right informations
            let dic1 = elements[indexPath.row]
            let attributedString = NSMutableAttributedString(string: dic1["name"].stringValue.uppercased(), attributes: [NSFontAttributeName:UIFont(name: "District-Book", size: 12)!, NSForegroundColorAttributeName: UIColor.black]
            )
            
            attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2), range: NSRange(location: 0, length: attributedString.length))
            
            cell.lbName.attributedText = attributedString
            
            cell.fnCreateSubViews(arrIn: dic1["value"].arrayValue, withIndexPath: indexPath)
            
            if (indexPath.row == 1) {
                let color1 = UIColor(rgb: 0xF1F1F1)
                
                cell.backgroundColor = color1
            }
            
            cell.completeClick = { (myDiction) in
                
                if (indexPath.row == 1) {
                    //4. When I tap on the qty for my own store, no need popup Out Going Message
                    return
                }
                
                let dic = JSON(object: myDiction)
                if dic["indexpath"].numberValue == 0 {
                }else{
                    self.storeid_inventory_selected = dic["store_id"].stringValue
                    
                    print(dic["store_id"].stringValue)
                    print(dic["indexpath"].stringValue)
                    let selectedProduct = UserDefaults.standard.string(forKey: "selectedProduct")
                    let newString = selectedProduct?.replacingOccurrences(of: ".json", with: "", options: .literal, range: nil)
                    self.my_product_id = newString!
                    let firstObj = self.elements[0]
                    self.valSize.text =  "SIZE: " + firstObj["value"].arrayValue[ dic["order"].intValue ]["stock"].stringValue.uppercased()
                    self.my_product_model = firstObj["value"].arrayValue[ dic["order"].intValue ]["model"].stringValue
                    self.valOutlet.text =  "OUTLET: " + dic["store_name"].stringValue.uppercased()
                    
                    var attributedString = NSMutableAttributedString(string: (self.valSize.text?.uppercased())!, attributes: [NSFontAttributeName:UIFont(name: "District-Book", size: 10)!, NSForegroundColorAttributeName: UIColor.black]
                        
                    )
                    attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
                    
                    self.valSize.attributedText = attributedString
                    
                    //
                    attributedString = NSMutableAttributedString(string: (self.valOutlet.text?.uppercased())!, attributes: [NSFontAttributeName:UIFont(name: "District-Book", size: 10)!, NSForegroundColorAttributeName: UIColor.black]
                        
                    )
                    attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
                    
                    self.valOutlet.attributedText = attributedString
                    
                    
                    self.fnShowSendingMessage()
                }
                
            }
            // Returning the cell
            return cell
            
        }
        
        //Related Table Color
        let cell = tbRelatedColor.dequeueReusableCell(withIdentifier: relatedIdentifier, for: indexPath) as! CellRelated1;
        
        cell.btnRelated.setTitle( self.arrTexts[indexPath.row] , for: .normal)
        cell.btnRelated.tag = indexPath.row
        
        cell.btnRelated.addTarget(self, action: #selector(self.pressButton(button:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == tbRelatedColor) {
            let dic =  self.arrRelatedProducts[indexPath.row]
            print(dic)
            DispatchQueue.main.async {
                self.fnClick(dic: dic)
            }
            
        }
        
    }
}
