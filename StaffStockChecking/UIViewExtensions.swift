import UIKit

extension UIView {

    func screenShotMethod() -> UIImage {
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot!
    }
    
    func slideInFrom( isLeft:Bool = true  ,_ duration: TimeInterval = 0.2, completionDelegate: CAAnimationDelegate? = nil) {
        
		// Create a CATransition animation
		let slideInFromLeftTransition = CATransition()
		
		// Set its callback delegate to the completionDelegate that was provided (if any) 
		if let delegate: CAAnimationDelegate = completionDelegate {
			slideInFromLeftTransition.delegate = delegate
		}
		
		// Customize the animation's properties
		slideInFromLeftTransition.type = kCATransitionReveal
        
        slideInFromLeftTransition.subtype =  isLeft ? kCATransitionFromLeft : kCATransitionFromRight
        
		slideInFromLeftTransition.duration = duration
		slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		slideInFromLeftTransition.fillMode = kCAFillModeBoth
		
		// Add the animation to the View's layer
		self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
	}
}
