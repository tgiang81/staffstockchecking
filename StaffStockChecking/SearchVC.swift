//
//  SearchVC.swift
//  StaffStockChecking
//
//  Created by giangtu on 8/6/17.
//  Copyright © 2017 Hemang Shah. All rights reserved.
//

import UIKit
import ACProgressHUD_Swift
import SwiftyJSON

class SearchVC: UIViewController {
    
    @IBOutlet weak var vCamera: UIView!
    @IBOutlet weak var vKeyboard: UIView!
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnKeyboard: UIButton!
    @IBOutlet weak var tfSKU: UITextField!
    @IBOutlet weak var labelSKU: UILabel!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var imgvCamera: UIImageView!
    @IBOutlet weak var imgvKeyboard: UIImageView!
    
    let queryService = APIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        var attributedString = NSMutableAttributedString(string: labelSKU.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        labelSKU.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: (buttonSearch.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        
        buttonSearch.setAttributedTitle(attributedString, for: .normal)

    }
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //When I logged in and go to search screen, show the barcode scan screen directly.
        //fnCamera([])

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fnCamera(_ sender: Any) {
        
        imgvCamera.image = UIImage.init(named: "ic_camera")
        imgvKeyboard.image = UIImage.init(named: "ic_keyboard_inactive")
        
        vCamera.isHidden = false
        vKeyboard.isHidden = true
        
        let controller = ScannerViewController(nibName: "ScannerViewController", bundle: nil)
        controller.delegate = self
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func fnKeyboard(_ sender: Any) {
        imgvCamera.image = UIImage.init(named: "ic_camera_inactive")
        imgvKeyboard.image = UIImage.init(named: "ic_keyboard")

        vCamera.isHidden = true
        vKeyboard.isHidden = false
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func fnSearchSKU(_ sender: Any) {
        //Search API...have result...show ProductVC
        if tfSKU.text != "" {
            fnSearchWithText(name: tfSKU.text!)
        }
    }
    
    func fnSearchWithText(name: String) {
       
        let trimmedString =  name.trimmingCharacters(in: NSCharacterSet.whitespaces)

        if( trimmedString == "" )
        { return }
        
        let progressView = ACProgressHUD.shared
        progressView.progressText = "Searching..."
        progressView.showHUD()
        ACProgressHUD.shared.hideHUD()
        
        queryService.getProductFind(searchTerm: trimmedString, completion: { (results) in
            //Check error code/???
            
            //hide progress
            ACProgressHUD.shared.hideHUD()
            
            print(results)
            
            let arr = results["products"].arrayValue
            //Search successful -> Navigate to Product
            if (arr.count > 0)
            {
                //take the first product to view
                let json = arr[0].dictionaryValue
                
                guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
                
                let fileUrl = documentsDirectoryUrl.appendingPathComponent(  Constants.APIDetails.FOLDER_PRODUCT + "/" + (json["id"]?.stringValue)! + ".json")
                
                UserDefaults.standard.set( (json["id"]?.stringValue)! + ".json" , forKey: "selectedProduct")
                
                print(fileUrl.absoluteString);
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: arr[0].dictionaryObject ?? [] , options: [])
                    try data.write(to: fileUrl, options: [])
                } catch {
                    print(error)
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.referenceUITabBarController.selectedIndex = 1
            }else{
            //
                let alert = UIAlertController(title: "Error", message: "The product you trying to search is not exists.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

        } )
    }
}

extension SearchVC: ScannerViewControllerDelegate, UITextFieldDelegate {
    func showKeyboard() {
        fnKeyboard([])
    }

    
    func finishScanning(_ scanCode: String){
        fnSearchWithText(name: scanCode)
    }
    
    // For pressing return on the keyboard to dismiss keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        fnSearchSKU(textField)
        return true
    }
}
