//
//  MessageVC.swift
//  StaffStockChecking
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class MessageVC: UIViewController {
    
    @IBOutlet weak var tableControl: UITableView!
    
    fileprivate let estadeCellIdentifier = "estadeIdentifier"
    
    //If not an Array or nil, return []
    var elementsList:  [[String:String]] = []
    
    var isBusy : Bool = false
    
    //messageid: APIManager
    
    var dicRequests : [String:APIManager] = [:]
    
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        
        self.tableControl.register(UINib.init(nibName: "CellMessage", bundle: nil), forCellReuseIdentifier: estadeCellIdentifier);
        
        self.tableControl.rowHeight = UITableViewAutomaticDimension
        self.tableControl.estimatedRowHeight = 60
        //timer request every 5 seconds
        //first time, something to show
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isBusy = false
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(doRefresh), userInfo: nil, repeats: true)
        }
        
        doRefresh()

    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isBusy = true
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }

    }
    
    func doRefresh() {
        //Busy do nothing

        if (isBusy)
        {
              return
        }

        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            self.elementsList.removeAll()
            self.tableControl.reloadData()

            return }
        let user = UserDefaults.standard.string(forKey: "user")
        
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("\(user!)_message.json")
        
        //Read saved id-status message
        do {
            self.elementsList.removeAll()
            
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let messageArr = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: String]] else {
                
                self.tableControl.reloadData()

                return }
            
            if messageArr.count > 0 {
                for i in 0...messageArr.count-1
                {
                    let obj = messageArr[i]
                    self.elementsList.append(obj)
                }
            }
            
            //get message data mapping with messageid list
            fireCellsUpdate()
            self.tableControl.reloadData()
            
        } catch {
            print(error)
        }
    }
    
    func fireCellsUpdate() {
        //stop query the server after the receiver_reply field is no longer 0, or user delete the cell
        
        if self.elementsList.count > 0
        {
            for idx in 0...self.elementsList.count-1 {
                
                var dic = self.elementsList[idx]
                
                //status = 0 => Stop request
                    var requestMain = APIManager()
                    var bExist = false
                    
                    if let request = dicRequests[ (dic["txn_id"])! ]{
                        requestMain = request
                        bExist = true
                    }
                    
                    requestMain.getMessage(messageID: (dic["txn_id"])!) { (results) in
                        //Check status...if change...update UI/file
                        
                        //save
                        if (!bExist)
                        {
                            self.dicRequests[ (dic["txn_id"])! ] = requestMain
                        }
                        
                        print(results.dictionaryValue)
                        
//                        1. The message, when the json come back, and receiver_message = 1, please auto remove the message.

                        //set status and refresh
                        if(results["request"]["receiver_final_status"].stringValue == "1")
                        {
                            self.elementsList.remove(at: idx)
                            
                            //reload data at index
                            //save cache
                            self.saveCache()

                        }
                        else if(results["request"]["receiver_reply"].stringValue != "0")
                        {
                            dic["receiver_reply"] = results["request"]["receiver_reply"].stringValue
                            
                             self.elementsList[idx] = dic
                            
                            //reload data at index
                            //save cache
                            self.saveCache()
                        }
                        
                        
                    }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func saveCache() {
        //update cache
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let user = UserDefaults.standard.string(forKey: "user")
        
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("\(user!)_message.json")

        do {
            let data = try JSONSerialization.data(withJSONObject: self.elementsList, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
}

extension MessageVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        isBusy = true
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.isBusy = false
            //update UI
            
            let dic = self.elementsList[indexPath.row]
            
            self.dicRequests[(dic["txn_id"])!] = nil
            
            
            self.elementsList.remove(at: indexPath.row)
            
            self.tableControl.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            //update cache
            self.saveCache()
        }
        
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        self.isBusy = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elementsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: estadeCellIdentifier, for: indexPath) as! CellMessage;
        cell.backgroundColor = UIColor.white;
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        
        let dic = self.elementsList[indexPath.row]
        
        if let photo_url = dic["image"], let url = URL(string: Constants.BASE_IMAGE_URL + photo_url) {
            cell.m_Image.sd_setImage(with: url)
        }
        

        
        cell.m_Name.text =  dic["name"]?.uppercased()
        cell.m_Color.text =  dic["colour"]?.uppercased()
        cell.m_Size.text =  dic["size"]?.uppercased()
        cell.m_Westgate.text =  dic["outlet"]?.uppercased()
        //Get Info "Full Name"
        var attributedString = NSMutableAttributedString(string: cell.m_Name.text!, attributes: [NSFontAttributeName:UIFont(name: "DIN1451Com-Engschrift", size: 12)!, NSForegroundColorAttributeName: UIColor.black]
        
        )
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2), range: NSRange(location: 0, length: attributedString.length))

        cell.m_Name.attributedText = attributedString
        attributedString = NSMutableAttributedString(string: cell.m_Color.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        cell.m_Color.attributedText = attributedString
        
        
        attributedString = NSMutableAttributedString(string: cell.m_Size.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        cell.m_Size.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: cell.m_Westgate.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        cell.m_Westgate.attributedText = attributedString
        cell.m_Status.textColor = .black
        let strStatus = dic["receiver_reply"]

        switch strStatus {
        case "0"?:
            cell.m_Status.text = "Pending reply".uppercased()
        case "1"?:
            cell.m_Status.text = "No Stock".uppercased()
            cell.m_Status.textColor = .red
        case "2"?:
            cell.m_Status.text = "Yes New PC".uppercased()
            cell.m_Status.textColor = .magenta
        case "3"?:
            cell.m_Status.text = "Display Perfect".uppercased()
        case "4"?:
            cell.m_Status.text = "Display Not Perfect".uppercased()
            
        default: break
            
        }
        
        attributedString = NSMutableAttributedString(string: cell.m_Status.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        cell.m_Status.attributedText = attributedString
        
        return cell;
    }
    
    
}

