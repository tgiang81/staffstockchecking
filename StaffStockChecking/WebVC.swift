//
//  WebVC.swift
//  StaffStockChecking
//
//  Created by giangtu on 11/10/17.
//  Copyright © 2017 Hemang Shah. All rights reserved.
//

//Abnormal case: If offline? auto go back...if can not load the web => can go back?


import WebKit

class WebVC: UIViewController, WKNavigationDelegate{
    var webView: WKWebView!
    var product_id: String!
    var product_modal: String!
    
    @IBOutlet weak var vContent: UIView!

    var webConfig: WKWebViewConfiguration {
        get {
            let webCfg = WKWebViewConfiguration()
            return webCfg;
        }
    }
    
    @IBAction func fnBack(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // http://www.theeditorsmarket.com/pos-web-view/waitlist/<product id>/<product model>
        // http://www.theeditorsmarket.com/pos-web-view/waitlist/5944/061117004scxs
        
        webView = WKWebView(frame: .zero, configuration: webConfig)
        webView.uiDelegate = self as? WKUIDelegate
        self.webView!.navigationDelegate = self

        let strLink = "http://www.theeditorsmarket.com/pos-web-view/waitlist/" + product_id

        let url = URL(string: strLink)!
        webView.load(URLRequest(url: url))
        
        webView.allowsBackForwardNavigationGestures = true
        
        vContent.addSubview(webView)
        
        webView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: webView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: vContent, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0).isActive = true
        
        NSLayoutConstraint(item: webView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: vContent, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0).isActive = true

        NSLayoutConstraint(item: webView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: vContent, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0).isActive = true
        
        NSLayoutConstraint(item: webView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: vContent, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0).isActive = true

        
    }
    
}


