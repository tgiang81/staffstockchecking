//
//  AccountVC.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {
    @IBOutlet weak var lbUserName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbUserName.text = Constants.userName
        view.setNeedsDisplay()

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fnLogout(_ sender: Any) {
        //Clear cache
        UserDefaults.standard.removeObject(forKey: "store_id")
        UserDefaults.standard.removeObject(forKey: "selectedProduct")
        UserDefaults.standard.removeObject(forKey: "usertoken")

        let fileManager = FileManager.default
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let folder = documentDirectory.appending("/" + Constants.APIDetails.FOLDER_PRODUCT)
        let fileNames = try! FileManager.default.contentsOfDirectory(atPath: folder )

        for fileName in fileNames {
            print(fileName)
            let filePath = folder.appending( "/" + fileName)
            if(fileManager.fileExists(atPath: filePath))
            {
                print("File abc exists")
                try! fileManager.removeItem(atPath: filePath)
            }
        }
        
        //show login
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.fnShowLogin()
    }
}
