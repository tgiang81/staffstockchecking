//
//  CustomTabbarController.swift
//
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.
//

import Foundation
import UIKit

class CustomTabbarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Creating a storyboard reference
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        
        let searchController = ScannerViewController(nibName: "ScannerViewController", bundle: nil)

        
        let navigationController1: UINavigationController = UINavigationController.init(rootViewController: searchController)
        navigationController1.title = "SEARCH"

        navigationController1.tabBarItem.selectedImage = UIImage.init(named: "search")?.withRenderingMode(.alwaysOriginal)
        navigationController1.tabBarItem.image = UIImage.init(named: "search_inactive")?.withRenderingMode(.alwaysOriginal)

        //Creating navigation controller for navigation inside the second tab.
        let navigationController2: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "ProductsVCID"))
        navigationController2.title = "PRODUCTS"
        navigationController2.tabBarItem.selectedImage = UIImage.init(named: "product")?.withRenderingMode(.alwaysOriginal)
        navigationController2.tabBarItem.image = UIImage.init(named: "product_inactive")?.withRenderingMode(.alwaysOriginal)

        let navigationController3: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "MessageVCID"))
        navigationController3.title = "MESSAGES"
        navigationController3.tabBarItem.selectedImage = UIImage.init(named: "message")?.withRenderingMode(.alwaysOriginal)
        navigationController3.tabBarItem.image = UIImage.init(named: "message_inactive")?.withRenderingMode(.alwaysOriginal)

        let navigationController4: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "AccountVCID"))
        navigationController4.title = "ACCOUNT"
        navigationController4.tabBarItem.selectedImage = UIImage.init(named: "account")?.withRenderingMode(.alwaysOriginal)
        navigationController4.tabBarItem.image = UIImage.init(named: "account_inactive")?.withRenderingMode(.alwaysOriginal)

        
        //text
        let color1 = UIColor(rgb: 0x000000)
        let color2 = UIColor(rgb: 0x999999)

        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : color2], for: UIControlState.normal) // changes the default color
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : color1], for: UIControlState.selected) // changes the default color
        
        //Update referenced TabbarController with your viewcontrollers
        viewControllers = [navigationController1 ,navigationController2, navigationController3, navigationController4]
        tabBar.isTranslucent = false
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.5)
        //topBorder.backgroundColor = UIColor.returnRGBColor(r: 229, g: 231, b: 235, alpha: 1).cgColor
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true

    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
