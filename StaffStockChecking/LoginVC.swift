//
//  LoginVC.swift
//
//  Created by Giang on 7/17/17.
//  Copyright © 2017 Giang. All rights reserved.

import UIKit
import SwiftyJSON
import ACProgressHUD_Swift

/*
 API End Point: https://api.shopcada.com/v1/
 
 Staff Login
 Registration code: apikey-test
 username: shop1
 pass: qwerty
 */

class LoginVC: UIViewController {
    
    @IBOutlet weak var lbRegistration: UILabel!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var lbPassword: UILabel!
    
    @IBOutlet weak var tfRegistrationCode: UITextField!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    let queryService = APIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var attributedString = NSMutableAttributedString(string: lbRegistration.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        lbRegistration.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: lbUsername.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        lbUsername.attributedText = attributedString

        attributedString = NSMutableAttributedString(string: lbPassword.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))
        lbPassword.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: (btnLogin.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(2.4), range: NSRange(location: 0, length: attributedString.length))

        btnLogin.setAttributedTitle(attributedString, for: .normal)
   
        //If don't see APIKEY, show Registration code
        if UserDefaults.standard.value(forKey: "APIKEY") != nil {
            tfRegistrationCode.isHidden = true
            lbRegistration.isHidden = true
            
        }else{
            tfRegistrationCode.isHidden = false
            lbRegistration.isHidden = false
        }
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfUsername.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fnLogin(_ sender: Any) {
        //If login successful => Save RegistrationCode
        
        let progressView = ACProgressHUD.shared
        progressView.progressText = "Please wait..."
        progressView.showHUD()
        
        var apikey = UserDefaults.standard.string(forKey: "APIKEY")

        if((apikey == nil) || apikey == "")
        {
            apikey = "Apikey " + self.tfRegistrationCode.text!
        }

        //hardcode
//        tfUsername.text = "user1"
//        tfPassword.text = "qwerty"
        
        
        queryService.getLogin(withAPIKey: apikey! ,username: tfUsername.text!, password: tfPassword.text! ) { results in
            
            ACProgressHUD.shared.hideHUD()
            
            if results["token"].string != nil  {
                
                print(results["token"].stringValue)
                
                
                UserDefaults.standard.set( self.tfUsername.text, forKey: "user")

                UserDefaults.standard.set(results["token"].stringValue, forKey: "usertoken")
                
                //"image_host": "https://www.shopcada.com/"
                Constants.BASE_IMAGE_URL = results["image_host"].stringValue
                //login successful!
                //Save APIKEY
                
                Constants.userName = (self.tfUsername?.text)!
                
                let apikey = UserDefaults.standard.string(forKey: "APIKEY")
                
                if((apikey == nil) || apikey == "")
                {
                    UserDefaults.standard.set("Apikey " + self.tfRegistrationCode.text!, forKey: "APIKEY")
                }

                UserDefaults.standard.set( results["store_id"].stringValue, forKey: "store_id")

                UserDefaults.standard.synchronize()

                //GO DASHBOARD
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.fnShowHome()
            }else{
                let alert = UIAlertController(title: "Error",
                                              message: "Invalid Username or Password. Please try again.",
                                              preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: "OK",
                                                 style: .cancel, handler: nil)
                
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        
    }
    
    /*
     // MARK: - Navigation
     */
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //view.endEditing(true)
        
    }
}

extension LoginVC: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        if textField == self.tfPassword {
            fnLogin(textField)
        }
        return true
    }
}

